#!/bin/bash

cd /home/alliedmodders/sourcemod/extensions/sm-ext-soundlib2/

cd    thirdparty/taglib &&
mkdir build -p &&
cd    build &&

CFLAGS="-m32" CXXFLAGS="-m32" cmake -DCMAKE_INSTALL_PREFIX=/home/alliedmodders/sourcemod/extensions/sm-ext-soundlib2/thirdparty/taglib/build \
      -DCMAKE_BUILD_TYPE=Release  \
      -DBUILD_SHARED_LIBS=OFF \
      -DENABLE_STATIC_RUNTIME=ON \
      ..

CFLAGS="-m32" CXXFLAGS="-m32" make -j 8

make install

cd /home/alliedmodders/sourcemod/extensions/sm-ext-soundlib2/

cd    thirdparty/zlib &&
mkdir build -p &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/home/alliedmodders/sourcemod/extensions/sm-ext-soundlib2/thirdparty/zlib/build \
      -DCMAKE_BUILD_TYPE=Release  \
      -DBUILD_SHARED_LIBS=OFF \
      -DENABLE_STATIC_RUNTIME=ON \
      ..

CFLAGS="-m32" CXXFLAGS="-m32" make -j 8

make install
